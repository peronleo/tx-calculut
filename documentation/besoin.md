# Étude du besoin

Ces documents ([QiSii](#qisii), [diagramme FAST](#diagramme-fast) et [note de clarification](#note-de-clarification)) n'ont pas été finalisés mais sont une trace importante du travail effectué pour l'étude du besoin.

Vous trouverez notamment le périmètre final de la TX et les fonctionnalités devant être développées ce semestre dans la [note de clarification](#note-de-clarification).

## QiSii

### Verbatim

Le BDE souhaite accompagner les associations de l'UTC dans une démarche responsable, dans l'adoption de la charte RSE et les pousser à s'engager plus activement sur les transformations qu'elles peuvent prendre pour améliorer leurs impacts environnementaux et sociaux. Il s'agit de permettre à ces associations de réaliser elles-mêmes et facilement les calculs d'impacts de leurs activités.

Deux axes (donc potentiellement deux essences en diagramme FAST) se dégagent de ce verbatim :

- accompagner
- réaliser des calculs d'impacts

### Question initiale posée

Comment permettre aux associations de l'UTC de calculer les impacts de leurs activités simplement et d'être accompagnées dans leurs transformations ?

### Solution initiale

- création d'un outil plus simple qu'un excel de calcul d'impact (carbone par exemple) permettant des recommandations simples, économiques, efficaces et priorisées à mettre en œuvre
- l'utilisateur doit être guidé et orientées par thématiques (Transports, Alimentation, Logistique, Activités)
- s'appuyant sur des données scientifiques (crédibilité, source)
- accessible aux associations de l'UTC pour réaliser des calculs
- accès administrateur BDE pour accompagner les associations / création d'un lien entre pôle RSE BDE et toutes les associations ou rediriger les associations vers le BDE pour se faire accompagner

### Questions pour la reformulation

- le format du parcours guidé est-il ferme ?
- les 4 grandes thématiques (transports, alimentation, logistique, activités) sont-elles fermes, vouées à évoluer ou à soumettre à un benchmark ?

### Remise en cause

- carte blanche pour le format, l'essentiel est la simplicité de l'outil
- les thématiques peuvent être amenées à évolution à la suite d'un inventaire des postes d'émissions.

### Conclusion

Le sujet de la TX est déjà plutôt très clair dans sa formulation initiale. Nous pouvons le préciser avec une note de clarification.

## Diagramme FAST

![](/resources/FASTdiagram.jpg)

## Note de clarification

- [Contexte](#contexte)
- [PRC-NRC (Périmètre et niveau de remise en cause)](#prc-nrc-périmètre-et-niveau-de-remise-en-cause)
  - [PRC : Périmètre de remise en cause](#prc--périmètre-de-remise-en-cause)
  - [HPRC : Hors périmètre de remise en cause](#hprc--hors-périmètre-de-remise-en-cause)
  - [NRC : Niveau de remise en cause](#nrc--niveau-de-remise-en-cause)
- [Clarification du vocabulaire utilisé](#clarification-du-vocabulaire-utilisé)
- [Périmètre](#périmètre)
- [Hors périmètre](#hors-périmètre)
- [Objectifs et livrables](#objectifs-et-livrables)
  - [Objectifs](#objectifs)
  - [Livrables](#livrables)
- [Indications du bilan carbone au format PDF](#indications-du-bilan-carbone-au-format-pdf)
- [Organisation et planification](#organisation-et-planification)
  - [Début du projet](#début-du-projet)
  - [Fin du projet](#fin-du-projet)
  - [Rétroplanning](#rétroplanning)
- [Acteurs](#acteurs)
  - [Commanditaire](#commanditaire)
  - [Développeurs](#développeurs)
  - [Encadrants](#encadrants)
- [Conséquences attendues](#conséquences-attendues)
- [Difficultés prévisibles](#difficultés-prévisibles)
- [Budget](#budget)

### Contexte

Le BDE souhaite accompagner les associations de l'UTC dans une démarche responsable, dans l'adoption de la charte RSE et les pousser à s'engager plus activement sur les transformations qu'elles peuvent prendre pour améliorer leurs impacts environnementaux et sociaux. Il s'agit de permettre à ces associations de réaliser elles-mêmes et facilement les calculs d'impacts de leurs activités.

Léo Péron et Julie Chartier sont missionnés dans le cadre d’une TX encadrée par Benjamin Lussier et Valentin Le Gauche afin de réaliser un outil de calcul d’impact des associations UTCéennes.

Nous faisons le choix de nous concentrer sur l’impact carbone des associations.

### PRC-NRC (Périmètre et niveau de remise en cause)

#### PRC : Périmètre de remise en cause

Liste de ce qui peut être modifié.

- Source des données (BDD ou API)

#### HPRC : Hors périmètre de remise en cause

- Technologies de développement du SIMDE (React.js et Laravel)
- Accompagnement des associations par le BDE (échanges nécessaires) car l’outil ne cherche pas à “tout faire”. L'application ne remplace pas l'humain et n'est pas totalement autonome.

#### NRC : Niveau de remise en cause

### Clarification du vocabulaire utilisé

Attention : pour consulter la terminologie finale relative au projet, veuillez vous référer à la [présentation de l'application](/README.md).

#### Événement

Désigne toute activité organisée par une association à une date ou période donnée et à destination d’un public.

_Par exemple :_ une soirée, un atelier, une projection d’un film, un festival.
_Exemples de ce qui n’est pas considéré comme un événement :_ une réunion de préparation.

#### Empreinte liée à un événement

Désigne toute consommation carbone associée à la création de l’événement, sa logistique et qui rentre dans l’un des postes d’émission définis par le BDE dans l’application (exemples : transport, alimentation, matériaux et consommables, numérique, sources fixes, déchets).

_Par exemple :_ transport des membres de l’association le jour J, transport du matériel en amont de l’événement, démontage après l’événement, empreinte carbone des aliments consommés par le public et les membres de l’association organisatrice durant l’événement.

_Exemples de ce qui n’est pas considéré comme une empreinte liée à un événement :_ impact du numérique utilisé lors des réunions régulières de l’association, nourriture commandée lors de rassemblement des membres de l’association.

#### Secteur d'émission

#### Bilan

### Périmètre

Dans le contexte de cette TX concentrerons sur le calcul de l’empreinte carbone d’un événement et laisserons la possibilité de faire évoluer l’outil afin d’intégrer le calcul du fonctionnement courant d’une association lors d’une autre TX par exemple.
Dans cette version de l’outil, nous intégrerons :

- Calcul de l’empreinte carbone
- Bilan carbone d’un événement
- Un accès administrateur
- Un accès membre associatif
- Affichage de la source des données

### Hors périmètre

Cette partie permet de lister les fonctionnalités auxquelles nous avons pensé mais n’implémenterons pas dans cette version de l’outil. L’intérêt de les lister permet de connaître les limites du projet mais aussi de fournir les idées d’améliorations possibles pour de futurs projets. Enfin, la prise en compte de ces possibles futures améliorations permet de les prendre en compte dans la conception du produit de manière à ce que leur implémentation soit aisée.
Dans cette version de l’outil, nous n’intégrerons pas :

- Calcul d’impacts autres que l’empreinte carbone tels que O2, biodiversité, CH4, etc.
- Bilan carbone du fonctionnement courant d’une association
- Comparaison des bilans
- Recommandations

### Objectifs et livrables

#### Objectifs

Livrer une application web qui puisse être utilisée à la fin du semestre pour réaliser un bilan carbone exemple et obtenir des recommandations cohérentes.
Les associations pourront identifier facilement les transformations à entreprendre avec l’appui du BDE.

#### Livrables

##### Application web

Deux rôles permettants deux affichages distincts :

- membre associatif
- administrateur (BDE par exemple)
  Elle sera composée de la manière suivante.

Version membre associatif :

- d’une page d’accueil sous forme de dashboard
- d’une page permettant d’effectuer un bilan carbone en remplissant un formulaire. Lors de la finalisation/dépôt du bilan carbone, un e-mail est automatiquement envoyé au BDE.
- une page permettant de consulter les résultats d’un bilan carbone d’un événement présentés sous forme de graphique par secteur d’émission avec la liste des émissions par type d’émission ou activité et un bouton permettant d’exporter en PDF le bilan. Un bouton permettant de modifier le bilan même s’il est finalisé/déposé. Un bouton permettant de supprimer un bilan tant qu’il n’est pas finalisé/déposé et en demandant une confirmation à l’utilisateur. Un bouton permettant de finaliser/déposer le bilan et qui envoie un e-mail au BDE. + le comparer avec un autre bilan
- une page permettant de consulter la liste des bilans effectués des semestres et éditions précédentes. Actions sur chaque bilan : boutons modifier/supprimer/finaliser.
- une page permettant de visualiser le bilan total (cumulé) de l’association
- une section ou page permettant l’accès aux contacts du BDE, la charte RSE, guide de l’événement soutenable, ressources documentaires pour réduire son impact carbone (source ADEME ou labels).

Version administrateur :

- d’une page d’accueil sous forme de tableau de bord
- d’un outil permettant modifier/ajouter/archiver les données de consommation utilisées par l’application. De source ADEME ou ajoutées par le BDE.
- une page permettant l’édition et la création des formulaires / questions que les membres associatifs doivent compléter afin de faire un bilan carbone
- une page permettant de voir toutes les associations et leurs bilans carbone chiffrés en eq CO2. Les classer par pôles d’associations
- une page permettant de consulter un bilan carbone (et son historique de versions) d’un événement d’une association (avec les graphiques, bilan chiffré, PDF et réponses aux formulaires) et un bouton permettant de modifier le bilan de l’association (validation qui envoie un e-mail à l’association avec l’ancien et le nouveau bilan).

### Indications du bilan carbone au format PDF

L’objectif est de pouvoir soumettre un pdf à des labellisateurs donc le BDE nous a orientés vers le rendu de l'outils ADERE de l'ADEME (parcours confirmé) : https://evenementresponsable.ademe.fr.

### Organisation et planification

#### Début du projet

20 février 2024

#### Fin du projet

02 juillet 2024

#### Rétroplanning

- Lancement du projet et définition du besoin (1 mois et demi)
- Conception Maquette (1 mois)
- Conception BDD (2 semaines)
- Développement (1 mois et demi)

### Acteurs

#### Commanditaire

- pôle RSE du BDE UTC

#### Développeurs

- Léo PÉRON, GI05
- Julie Chartier, GI02

#### Encadrants

- Benjamin Lussier
- Valentin Le Gauche

### Conséquences attendues

### Difficultés prévisibles

- Pas de méthode universelle pour le calcul de l’empreinte carbone.
- Mise à disposition des données
- Gestion de connexion CAS et rôles

### Budget

Non concerné.
