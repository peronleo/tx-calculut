# Calcul'UT

## Table des matières

- [Contexte](#contexte)
- [L'application](#lapplication)
  - [Partie Administrateur](#partie-administrateur)
  - [Partie Association/Utilisateur](#partie-associationutilisateur)
- [Définitions des concepts de l'application](#définitions-des-concepts-de-lapplication)
- [Installation du projet](#installation-du-projet)
  - [Prérequis](#prérequis)
  - [Environnement de développement](#environnement-de-développement)
- [Architecture Projet et BDDs](#architecture-projet-et-bdds)
  - [Structure du projet](#structure-du-projet)
  - [Architecture BDD](#architecture-bdd)
- [Documentation de l'API](#documentation-de-lapi)
  - [Lister les routes](#lister-les-routes)
- [JSONs](#jsons)

## Contexte

Le BDE-UTC, fédération des associations UTCéennes, a émis une demande quant à la réalisation d'un outil permettant d'estimer le bilan carbonne des activités de ses associations.

Un sujet de TX a été créé afin de développer la solution demandée par le BDE lors du semestre P24.

Lors du semestre P24, une [étude du besoin](/documentation/besoin.md), une [conception](/documentation/conception.md) et un MVP ont été réalisés. Calcul'UT reste pour le moment un outil expérimental et nous encourageons le BDE à poursuivre la TX afin de le rendre plus consistant et déployable. Cependant, l'outil peut être testé pour différentes utilisations afin d'avoir des premiers feed-backs dans le but d'instaurer une logique d'amélioration continue.

Deux "issue boards" sont présents dans le repository Gitlab permettant de gérer le développement des fonctionnalités de l'application et les bugs à corriger.

## L'application

Calcul'UT est une application de formulaire dynamique et modulable, elle permet au BDE-UTC de créer des formulaires liés à des pôles d'émissions carbones qui pourront ensuite être complétés par les différentes associations UTCéennes.

### Partie Administrateur

La patie Administrateur permet au BDE-UTC de créer des formulaires, des données, et de visualiser les différents bilans d'émissions carbones de ses associations.

### Partie Association/Utilisateur

La patie Association/Utilisateur permet à un membre d'une association de créer un bilan carbone pour chaque événement qu'elle organise et de visualiser son impact carbone.

## Définitions des concepts de l'application

### Événement

Un événement représente un événement organisé par une association. Ses attributs sont l'intitulé de l'événement, une date de début et une date de fin.

### Bilan

Un bilan est relié à un événement. Il contient les réponses de l'association aux formulaires qu'elle a sélectionné. Une fois le bilan finalisé, la page de visualisation du bilan devient disponible. Un bilan peut avoir plusieurs versions. Il peut être modifié et vu par le BDE une fois finalisé.

### Formulaire

Un formulaire comprend un intitulé, un secteur, une description, une formule et une liste de N questions.

### Question

Une question comprend un intitulé, une description, un nom de variable, un type (_saisie utilisateur_ ou _choix unique_) et N données associées si le type est _choix unique_. Chaque question créée et rattachée à un formulaire est soit de type :

- _saisie utilisateur_ : Une saisie numérique de l'utilisateur.
- _choix unique_ : Une question à choix unique qui permet à l'utilisateur de choisir une réponse parmis plusieurs données préalablement sélectionnées par l'admin. Chaque réponse possible est rattachée à une donnée créée préalablement par le BDE.

Le nom de variable d'une question est unique, permettant d'en faire référence lors de la création de la formule.

### Réponse

Une réponse est une donnée qui est référencée dans les choix possibles d'une question.

### Donnée

Une donnée est une valeur numérique avec unitée entrée par le BDE liée à un type d'émission. Chaque donnée est accompagnée d'une source afin d'être transparent et rigoureux dans les chiffres utilisés.

_Exemple de donnée:_ La donnée d'émission d'un BUS en kgCO2/p/km, la source d'information étant la base carbonne de l'Ademe.

### Formule

La formule du formulaire permet de créer le lien entre les différentes questions afin de calculer l'émission liée au sujet du formulaire.

_Exemple: Un formulaire a été créé pour estimer l'émission carbonne liée au transport des participants. Celui-ci est rattaché au secteur "Transport" et comprend 3 questions:_

- _"Quel type de transport a été utilisé ? Choix unique parmis: [Navette, Avion, Train,...]"_
- _"Combien de participants ont été transportés ? Saisie numérique de l'utilisateur"_
- _"Combien de KMs ont été parcourus ? Saisie numérique de l'utilisateur"_

_La formule étant "typeTransport * nbKm * nbpersonnes", il est possible de calculer l'émission liée au transport des participants._

## Installation du projet

### Prérequis

- php
- node.js

### Environnement de développement

Cloner le repository en local :

Avec SSH :

```
git clone git@gitlab.utc.fr:peronleo/tx-calculut.git
```

Ou avec HTTPS :

```
git clone https://gitlab.utc.fr/peronleo/tx-calculut.git
```

#### Configurer le docker-compose pour la BDD

Configurer le docker-compose pour une BDD Postgres ou utiliser sa propre BDD Postgres en local.

#### Démarrer l'API

```
php artisan migrate
php artisan serve
```

#### Démarrer les tests Laravel

```
php artisan test --testsuite=Unit
```

#### Installer les dépendances front-end

```
npm install
```

#### Démarrer l'application VueJS

```
npm run dev
```

## Architecture Projet et BDDs

### Structure du Projet

Nous allons expliquer la structure d'un projet Laravel et comment nous avons intégré Vue.js comme framework front-end.

#### Structure de Base d'un Projet Laravel

Un projet Laravel est organisé de manière à séparer les différentes responsabilités de l'application. Voici les principaux répertoires et fichiers :

- **app/** : Contient le code principal de l'application, y compris les modèles, les contrôleurs et les services.

  - **Http/Controllers/** : Contient les contrôleurs de l'application, qui gèrent les requêtes HTTP et renvoient des réponses.
  - **Models/** : Contient les modèles Eloquent, qui représentent les tables de la base de données.
  - **Services/** : Contient les services, qui configurent et initialisent les services de l'application.

- **config/** : Contient les fichiers de configuration de l'application.

  - **app.php** : Fichier de configuration principal de l'application.
  - **database.php** : Configuration de la base de données.

- **database/** : Contient les migrations, les seeders et les factories pour la base de données.

  - **migrations/** : Contient les fichiers de migration qui définissent la structure des tables de la base de données.
  - **seeders/** : Contient les classes qui permettent de peupler la base de données avec des données de test.

- **routes/** : Contient les définitions de routes de l'application.

  - **web.php** : Contient les routes web (accédées via un navigateur).
  - **api.php** : Contient les routes API (accédées via des appels API).

- **tests/** : Contient les tests unitaires et fonctionnels.
  - **Feature/** : Contient les tests fonctionnels.
  - **Unit/** : Contient les tests unitaires.

Pour plus d'informations, la documentation de Laravel est très exhaustive et a été de nombreuses fois utilisée lors de la réalisation de ce projet: [Laravel Documentation](https://laravel.com/docs/11.x)

#### Intégration de Vue.js dans Laravel

Pour le front-end, nous avons utilisé Vue.js, un framework JavaScript moderne qui facilite la création d'interfaces utilisateur dynamiques. Nous avons intégré Vue.js dans la partie `resources` de Laravel, sous le répertoire `resources/js`.

### Architecture BDD

![](/resources/bilan_formulaire.png)

![](/resources/UML%20v2.drawio.png)

## Documentation de l'API

Une collection Postman est disponible ici : [ressources](/resources/).

### Lister les routes

- Se placer dans le dossier calculut :

```
cd .\calculut\
```

- Lister les routes :

```
php artisan route:list
```

### Routes

#### Assos

**GET api/assos** : Récupérer les associations et le nom complet de l'étudiant

#### Bilans

**GET api/bilans** : Récupérer tous les bilans

**POST api/bilans** : Créer un bilan

**GET api/bilans/{bilan}** : Récupérer un bilan

**PUT api/bilans/{bilan}** : Modifier un bilan

**DELETE api/bilans/{bilan}** : Supprimer un bilan

**POST api/bilans/{bilan}/duplicate** : Dupliquer un bilan

#### Enregistrements d'un bilan

**GET api/bilans/{bilan}/enregistrements** : Récupérer les enregistrements d'un bilan

**POST api/bilans/{bilan}/enregistrements** : Créer un enregistrement d'un bilan

**GET api/bilans/{bilan}/enregistrements/{enregistrement}** : Récupérer un enregistrement d'un bilan

**PUT api/bilans/{bilan}/enregistrements/{enregistrement}** : Modifier un enregistrement d'un bilan

**DELETE api/bilans/{bilan}/enregistrements/{enregistrement}** : Supprimer un enregistrement d'un bilan

#### Données

**GET api/donnees** : Récupérer toutes les données

**POST api/donnees** : Créer une donnée

**GET api/donnees/{donnee}** : Récupérer une donnée

**PUT api/donnees/{donnee}** : Modifier une donnée

**DELETE api/donnees/{donnee}** : Supprimer une donnée

**POST api/donnees/{donnee}/duplicate** : Dupliquer une donnée

#### Formulaires

**GET api/formulaires** : Récupérer tous les formulaires

**POST api/formulaires** : Créer un formulaire

**GET api/formulaires/{formulaire}** : Récupérer un formulaire

**PUT api/formulaires/{formulaire}** : Modifier un formulaire

**DELETE api/formulaires/{formulaire}** : Supprimer un formulaire

**GET api/formulaires/{formulaire}/questions** : Récupérer les questions d'un formulaire

**POST api/formulaires/{formulaire}/questions** : Créer un question associée à un formulaire

**GET api/formulaires/{formulaire}/questions/{question}** : Récupérer une question associée à un formulaire

**PUT api/formulaires/{formulaire}/questions/{question}** : Modifier une question associée à un formulaire

**DELETE api/formulaires/{formulaire}/questions/{question}** : Modifier une question associée à un formulaire

**POST api/formulaires/{formulaire}/questions/{question}/donnees/{donnee}** : Associer une donnée à une question (dans les réponses possibles).

**DELETE api/formulaires/{formulaire}/questions/{question}/donnees/{donnee}** : Supprimer l'association d'une donnée à une question.

#### Session / authentification

**GET auth** : Route d'authentification

**GET logout** : Route de déconnexion

## JSONs

### Bilan

```json
{
  "id": "9c4ac956-7285-4383-a5f4-8ca503486d9b",
  "created_at": "2024-06-15T11:16:33.000000Z",
  "updated_at": "2024-06-30T13:59:18.000000Z",
  "deleted_at": null,
  "intitule": "test Copie",
  "type": "event",
  "auteur": "Julie Chartier",
  "asso": "BDE",
  "pole_asso": null,
  "enregistrement": {
    "auteur": "auteur",
    "date": "2024-06-30T13:59:18.556Z",
    "formulaires": [
      {
        "id": "9c4d315c-64ea-4a8a-af7b-06421b1fd7d5",
        "intitule": "Transport des intervenants",
        "description": "transport",
        "publie": true,
        "formule": {
          "type": "multiply-dot",
          "a": {
            "type": "variable",
            "name": "typeTransport"
          },
          "b": {
            "type": "variable",
            "name": "nbIntervenants"
          }
        },
        "evaluation": null,
        "secteur": "transport",
        "updated_at": "2024-06-16T16:00:44.000000Z",
        "questions": [
          {
            "id": "9c4d31a1-858b-486a-8cbc-c1571035ea83",
            "intitule": "Quel moyen de transport ?",
            "description": null,
            "variable": "typeTransport",
            "type": "unique",
            "donnees": [
              {
                "id": "9c4ad0df-1ca5-46b8-b1d0-166bed0e0b6e",
                "pivot": {
                  "donnee_id": "9c4ad0df-1ca5-46b8-b1d0-166bed0e0b6e",
                  "question_id": "9c4d31a1-858b-486a-8cbc-c1571035ea83"
                },
                "unite": "kgCO2/km/p",
                "source": "ADEME",
                "valeur": 235,
                "intitule": "Avion",
                "metrique": "CO2",
                "created_at": "2024-06-15T11:37:37.000000Z",
                "updated_at": "2024-06-15T11:37:37.000000Z",
                "description": null
              },
              {
                "id": "9c4ad1fe-bd88-4371-aff2-502ef9c42619",
                "pivot": {
                  "donnee_id": "9c4ad1fe-bd88-4371-aff2-502ef9c42619",
                  "question_id": "9c4d31a1-858b-486a-8cbc-c1571035ea83"
                },
                "unite": "kgCO2/km/p",
                "source": "ADEME",
                "valeur": 50,
                "intitule": "Bus",
                "metrique": "CO2",
                "created_at": "2024-06-15T11:40:46.000000Z",
                "updated_at": "2024-06-16T15:58:29.000000Z",
                "description": "test"
              }
            ],
            "reponse": 50
          },
          {
            "id": "9c4d31d1-97fa-4eb1-a77a-f9cb59015036",
            "intitule": "Combien d'intervenants ?",
            "description": null,
            "variable": "nbIntervenants",
            "type": "saisie",
            "donnees": [],
            "reponse": 10
          }
        ]
      }
    ]
  },
  "evenement": {
    "id": "9c4ac956-75fd-482d-8d61-2984a6c9ecc9",
    "created_at": "2024-06-15T11:16:33.000000Z",
    "updated_at": "2024-06-16T15:57:37.000000Z",
    "debut": 1718450193,
    "fin": 1718488800,
    "description": null,
    "bilan_id": "9c4ac956-7285-4383-a5f4-8ca503486d9b"
  },
  "formulaires": []
}
```

### Enregistrement

```json
{
  "auteur": "auteur",
  "date": "2024-06-30T13:59:19.899Z",
  "formulaires": [
    {
      "id": "9c4d315c-64ea-4a8a-af7b-06421b1fd7d5",
      "intitule": "Transport des intervenants",
      "description": "transport",
      "publie": true,
      "formule": {
        "type": "multiply-dot",
        "a": {
          "type": "variable",
          "name": "typeTransport"
        },
        "b": {
          "type": "variable",
          "name": "nbIntervenants"
        }
      },
      "evaluation": 500,
      "secteur": "transport",
      "updated_at": "2024-06-16T16:00:44.000000Z",
      "questions": [
        {
          "id": "9c4d31a1-858b-486a-8cbc-c1571035ea83",
          "intitule": "Quel moyen de transport ?",
          "description": null,
          "variable": "typeTransport",
          "type": "unique",
          "donnees": [
            {
              "id": "9c4ad0df-1ca5-46b8-b1d0-166bed0e0b6e",
              "pivot": {
                "donnee_id": "9c4ad0df-1ca5-46b8-b1d0-166bed0e0b6e",
                "question_id": "9c4d31a1-858b-486a-8cbc-c1571035ea83"
              },
              "unite": "kgCO2/km/p",
              "source": "ADEME",
              "valeur": 235,
              "intitule": "Avion",
              "metrique": "CO2",
              "created_at": "2024-06-15T11:37:37.000000Z",
              "updated_at": "2024-06-15T11:37:37.000000Z",
              "description": null
            },
            {
              "id": "9c4ad1fe-bd88-4371-aff2-502ef9c42619",
              "pivot": {
                "donnee_id": "9c4ad1fe-bd88-4371-aff2-502ef9c42619",
                "question_id": "9c4d31a1-858b-486a-8cbc-c1571035ea83"
              },
              "unite": "kgCO2/km/p",
              "source": "ADEME",
              "valeur": 50,
              "intitule": "Bus",
              "metrique": "CO2",
              "created_at": "2024-06-15T11:40:46.000000Z",
              "updated_at": "2024-06-16T15:58:29.000000Z",
              "description": "test"
            }
          ],
          "reponse": 50
        },
        {
          "id": "9c4d31d1-97fa-4eb1-a77a-f9cb59015036",
          "intitule": "Combien d'intervenants ?",
          "description": null,
          "variable": "nbIntervenants",
          "type": "saisie",
          "donnees": [],
          "reponse": 10
        }
      ]
    }
  ]
}
```

### Donnée

```json
{
  "intitule": "Nom de la donnée",
  "description": "Desc de la donnée",
  "valeur": 3.14,
  "unite": "kgC02/p/km",
  "source": "Ademe",
  "metrique": "CO2"
}
```

### Formulaire

```json
{
  "intitule": "Transport des intervenants",
  "description": "transport",
  "secteur": "transport",
  "formule": {
    "a": {
      "name": "typeTransport",
      "type": "variable"
    },
    "b": {
      "name": "nbIntervenants",
      "type": "variable"
    },
    "type": "multiply-dot"
  },
  "publie": true,
  "questions": []
}
```
